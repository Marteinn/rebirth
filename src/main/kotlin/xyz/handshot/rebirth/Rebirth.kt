package xyz.handshot.rebirth

import net.fabricmc.api.ModInitializer
import org.slf4j.LoggerFactory
import java.io.File

class Rebirth : ModInitializer {
    companion object {
        val LOGGER = LoggerFactory.getLogger("Rebirth")
        val FOLDER = File("config/Rebirth")
    }

    override fun onInitialize() {
    }
}